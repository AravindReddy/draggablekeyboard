﻿using Android.Content;
using Android.Content.Res;
using Android.Net;
using System;
using System.Security.Cryptography;

namespace FSI.Mobile.FL.Android.Utils
{
    class Util
    {
        public static bool IsNetworkAvailable(Context context)
        {
            ConnectivityManager connectivityManager
                  = (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);
            NetworkInfo activeNetworkInfo = connectivityManager.ActiveNetworkInfo;
            return activeNetworkInfo != null && activeNetworkInfo.IsConnected;
        }

        public static int GetRandomId()
        {
            var bytes = new byte[4];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
            return Convert.ToInt32(String.Format("{0:D8}", random));
        }

        public static int GetStatusBarHeight(Resources resources)
        {
            int result = 0;

            int resourceId = resources.GetIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0)
            {
                result = resources.GetDimensionPixelSize(resourceId);
            }
            return result;
        }
    }
}