﻿using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Media;
using Android.Util;
using IO = System.IO;
using OS = Android.OS;
using Media = Android.Media;
using Net = Android.Net;
using Android.Graphics.Drawables;
using Droid = Android.Support;

namespace FSI.Mobile.FL.Android.Utils
{
    public static class ImageUtils
    {
        public static async Task<BitmapFactory.Options> GetBitmapOptionsOfImageAsync(byte[] byteArray)
        {
            BitmapFactory.Options options = new BitmapFactory.Options
            {
                InJustDecodeBounds = true
            };
            // The result will be null because InJustDecodeBounds == true.
            Bitmap result = await BitmapFactory.DecodeByteArrayAsync(byteArray, 0, byteArray.Length, options);
            int imageHeight = options.OutHeight;
            int imageWidth = options.OutWidth;
            return options;
        }

        public static BitmapFactory.Options GetBitmapOptionsOfImage(byte[] byteArray)
        {
            BitmapFactory.Options options = new BitmapFactory.Options
            {
                InJustDecodeBounds = true
            };
            // The result will be null because InJustDecodeBounds == true.
            Bitmap result = BitmapFactory.DecodeByteArray(byteArray, 0, byteArray.Length, options);
            int imageHeight = options.OutHeight;
            int imageWidth = options.OutWidth;
            return options;
        }
        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            float height = options.OutHeight;
            float width = options.OutWidth;
            double inSampleSize = 1D;
            if (height > reqHeight || width > reqWidth)
            {
                int halfHeight = (int)(height / 2);
                int halfWidth = (int)(width / 2);

                // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
                while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }
            return (int)inSampleSize;
        }
        public static async Task<Bitmap> LoadScaledDownBitmapForDisplayAsync(byte[] byteArray, BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
            return await BitmapFactory.DecodeByteArrayAsync(byteArray, 0, byteArray.Length, options);
        }

        public static Bitmap LoadScaledDownBitmapForDisplay(byte[] byteArray, BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
            return BitmapFactory.DecodeByteArray(byteArray, 0, byteArray.Length, options);
        }

        public static int ConvertDpToPixel(double dp)
        {
            DisplayMetrics metrics = Resources.System.DisplayMetrics;
            double px = dp * ((int)metrics.DensityDpi / 160d);
            return (int)System.Math.Round(px);
        }

        public static void CopyPictureToDownloads(Context context, byte[] file, string fileName)
        {

            //Get Downloads directory
            var downloadDirectory = IO.Path.Combine(OS.Environment.ExternalStorageDirectory.AbsolutePath, OS.Environment.DirectoryDownloads);
            var filePath = IO.Path.Combine(downloadDirectory, fileName);

            // Create and save your file to the Android device
            var streamWriter = IO.File.Create(filePath);
            streamWriter.Close();
            IO.File.WriteAllBytes(filePath, file);

            // Notify the user about the completed "download"
            var downloadManager = DownloadManager.FromContext(context);
            downloadManager.AddCompletedDownload(fileName, "Description", true, "image/jpeg", filePath, IO.File.ReadAllBytes(filePath).Length, true);
        }
        public static Bitmap RotateImageIfRequired(Context context, Bitmap img, Net.Uri selectedImage)
        {
            var input = context.ContentResolver.OpenInputStream(selectedImage);
            if (img == null)
                return null;
            var exif = new ExifInterface(input);
            var rotation = exif.GetAttributeInt(ExifInterface.TagOrientation, (int)Media.Orientation.Normal);
            var rotationInDegrees = ExifToDegrees(rotation);
            if (rotationInDegrees == 0)
                return img;

            using (var matrix = new Matrix())
            {
                matrix.PreRotate(rotationInDegrees);
                return Bitmap.CreateBitmap(img, 0, 0, img.Width, img.Height, matrix, true);
            }
        }
        public static int ExifToDegrees(int exifOrientation)
        {
            switch (exifOrientation)
            {
                case (int)Media.Orientation.Rotate90:
                    return 90;
                case (int)Media.Orientation.Rotate180:
                    return 180;
                case (int)Media.Orientation.Rotate270:
                    return 270;
                default:
                    return 0;
            }
        }

        public static Drawable SetColorToDrawable(this Drawable drawable, int color)
        {
            Drawable customDrawable = Droid.V4.Graphics.Drawable.DrawableCompat.Wrap(drawable);
            customDrawable.Mutate();
            Droid.V4.Graphics.Drawable.DrawableCompat.SetTint(drawable, color);
            drawable.SetBounds(0, 0, drawable.IntrinsicWidth, drawable.IntrinsicHeight);
            return customDrawable;
        }
    }
}