﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using System.Collections.Generic;
using FSI.Mobile.FL.Android.Utils;
using Android.InputMethodServices;
using Droid = Android;
using System;

namespace keyboard
{
    [Activity(Label = "keyboard", MainLauncher = true)]
    public class MainActivity : DraggableKeyboardActivity
    {
        private KeyboardView _keyboardView;
        private RelativeLayout _keyboardLayout;
        private View _dragHandle;
        private View _mainLayout;
        private View _done;
        private int mainLayoutPadding = ImageUtils.ConvertDpToPixel(16);

        public override bool ActivityHasActionBar => true;

        public override string CurrentFocusedText { get; set; }

        public override int KeyBoardMarginRight => mainLayoutPadding;

        public override int KeyBoardMarginLeft => mainLayoutPadding;

        public override int KeyBoardMarginBottom => mainLayoutPadding;

        public override int KeyBoardMarginTop => mainLayoutPadding;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            InitKeyBoard();
        }

        private void InitKeyBoard()
        {
            _keyboardView = FindViewById<KeyboardView>(Resource.Id.keyboard_view);
            _keyboardLayout = FindViewById<RelativeLayout>(Resource.Id.keyboard_layout);
            _dragHandle = FindViewById(Resource.Id.drag_handle);
            _mainLayout = FindViewById(Resource.Id.main_layout);
            _done = FindViewById(Resource.Id.done);
            _done.Click += _done_Click;
            SetupKeyboard(_mainLayout, _keyboardView, _keyboardLayout, _dragHandle);
            SetKeybordVisiblity(ViewStates.Visible);
        }

        private void _done_Click(object sender, EventArgs e)
        {
            _keyboardLayout.Visibility = ViewStates.Gone;
        }
        
        public override string GetCharacterFromKeyCode(Droid.Views.Keycode keycode)
        {
            switch (keycode)
            {
                case Droid.Views.Keycode.Num0:
                    return "0";
                case Droid.Views.Keycode.Num1:
                    return "1";
                case Droid.Views.Keycode.Num2:
                    return "2";
                case Droid.Views.Keycode.Num3:
                    return "3";
                case Droid.Views.Keycode.Num4:
                    return "4";
                case Droid.Views.Keycode.Num5:
                    return "5";
                case Droid.Views.Keycode.Num6:
                    return "6";
                case Droid.Views.Keycode.Num7:
                    return "7";
                case Droid.Views.Keycode.Num8:
                    return "8";
                case Droid.Views.Keycode.Num9:
                    return "9";
                case Droid.Views.Keycode.Period:
                    return ".";
                default: return "";
            }
        }

        public override void SetText(string text)
        {

        }
    }
}

