﻿using Android.App;
using Android.InputMethodServices;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using FSI.Mobile.FL.Android.Utils;
using Java.Lang;
using static Android.Views.View;
using Droid = Android;

namespace keyboard
{
    [Activity(Label = "CustomKeyboardActivity")]
    public abstract class DraggableKeyboardActivity : Activity
    {
        private int windowWidth;
        private int windowHeight;
        private RelativeLayout keyboardContainer;
        private KeyBoardOnTouchListener touchListner;
        private MyKeyboardListener keyBoardListener;
        public abstract int KeyBoardMarginRight { get; }
        public abstract int KeyBoardMarginLeft { get; }
        public abstract int KeyBoardMarginBottom { get; }
        public abstract int KeyBoardMarginTop { get; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            windowWidth = Resources.DisplayMetrics.WidthPixels;
            windowHeight = Resources.DisplayMetrics.HeightPixels;
            if (ActivityHasActionBar)
            {
                // Calculate ActionBar's height 
                if (ActivityHasActionBar)
                {
                    TypedValue tv = new TypedValue();
                    if (Theme.ResolveAttribute(Droid.Resource.Attribute.ActionBarSize, tv, true))
                    {
                        windowHeight -= TypedValue.ComplexToDimensionPixelSize(tv.Data, Resources.DisplayMetrics);
                    }
                }
                windowHeight -= Util.GetStatusBarHeight(Resources);
            }
        }

        public abstract void SetText(string text);

        public void SetupKeyboard(View mainLayout, KeyboardView keyboardView, RelativeLayout keyboardContainer, View dragHandle)
        {
            keyboardView.Keyboard = new Keyboard(this, Resource.Xml.Keyboard);
            keyboardView.OnKeyboardActionListener = keyBoardListener = new MyKeyboardListener(this);
            touchListner = new KeyBoardOnTouchListener(this);
            touchListner.viewHeight = ImageUtils.ConvertDpToPixel(240 + 36);
            touchListner.viewWidth = ImageUtils.ConvertDpToPixel(240 + 36);
            touchListner._keyboardLayout = this.keyboardContainer = keyboardContainer;
            touchListner.windowHeight = windowHeight;
            touchListner.windowWidth = windowWidth;
            touchListner.mainLayout = mainLayout;
            dragHandle.SetOnTouchListener(touchListner);
        }
        public abstract bool ActivityHasActionBar { get; }
        public abstract string CurrentFocusedText { get; set; }

        public void SetKeybordVisiblity(ViewStates viewState)
        {
            if (keyboardContainer != null)
            {
                keyboardContainer.Visibility = viewState;
            }
        }

        //public void SetCurrentTextView(TextView textView)
        //{
        //    CurrentFocusedTextView = textView;
        //}
        public abstract string GetCharacterFromKeyCode(Droid.Views.Keycode keycode);

    }

    public class MyKeyboardListener : Java.Lang.Object, KeyboardView.IOnKeyboardActionListener
    {
        private readonly DraggableKeyboardActivity _activity;


        public MyKeyboardListener(DraggableKeyboardActivity activity)
        {
            _activity = activity;
        }

        public void OnKey([GeneratedEnum] Droid.Views.Keycode primaryCode, [GeneratedEnum] Droid.Views.Keycode[] keyCodes)
        {
            var text = _activity.CurrentFocusedText;
            if (primaryCode == Droid.Views.Keycode.Del)
            {
                if (text.Length > 0)
                {
                    text = text.Substring(0, text.Length - 1);
                }
            }
            else
            {
                var character = _activity.GetCharacterFromKeyCode(primaryCode);
                text = text + character;
            }
            _activity.CurrentFocusedText = text;
            _activity.SetText(text);
        }



        public void OnPress([GeneratedEnum] Droid.Views.Keycode primaryCode) { /*No Action*/ }
        public void OnRelease([GeneratedEnum] Droid.Views.Keycode primaryCode) { /*No Action*/ }
        public void OnText(ICharSequence text) { /*No Action*/ }
        public void SwipeDown() { /*No Action*/ }
        public void SwipeLeft() { /*No Action*/ }
        public void SwipeRight() { /*No Action*/ }
        public void SwipeUp() { /*No Action*/ }
    }

    class KeyBoardOnTouchListener : Java.Lang.Object, IOnTouchListener
    {
        private readonly DraggableKeyboardActivity _activity;
        public KeyBoardOnTouchListener(DraggableKeyboardActivity activity)
        {
            _activity = activity;
        }
        public int xDelta { get; set; }
        public int yDelta { get; set; }
        public View _keyboardLayout { get; set; }
        public int windowWidth { get; set; }
        public int viewWidth { get; set; }
        public int windowHeight { get; set; }
        public int viewHeight { get; set; }
        public View mainLayout { get; set; }
        public bool OnTouch(View v, MotionEvent e)
        {
            int x = (int)e.RawX;
            int y = (int)e.RawY;
            switch (e.Action & MotionEventActions.Mask)
            {

                case MotionEventActions.Down:
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)_keyboardLayout.LayoutParameters;

                    xDelta = x - lParams.LeftMargin;
                    yDelta = y - lParams.TopMargin;
                    break;

                case MotionEventActions.Up:

                    break;

                case MotionEventActions.Move:
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)_keyboardLayout.LayoutParameters;
                    int leftMargin = (x - xDelta);
                    int topMargin = (y - yDelta);

                    if (leftMargin < _activity.KeyBoardMarginLeft)
                    {
                        layoutParams.LeftMargin = 0;
                    }
                    else if (leftMargin >= (windowWidth - _activity.KeyBoardMarginRight - viewWidth))
                    {
                        layoutParams.LeftMargin = windowWidth - _activity.KeyBoardMarginRight - viewWidth;
                    }
                    else
                    {
                        layoutParams.LeftMargin = leftMargin;
                    }

                    if (topMargin < _activity.KeyBoardMarginTop)
                    {
                        layoutParams.TopMargin = 0;
                    }
                    else if (topMargin >= (windowHeight - _activity.KeyBoardMarginBottom - viewHeight))
                    {
                        layoutParams.TopMargin = windowHeight - _activity.KeyBoardMarginBottom - viewHeight;
                    }
                    else
                    {
                        layoutParams.TopMargin = topMargin;
                    }
                    layoutParams.RightMargin = 0;
                    layoutParams.BottomMargin = 0;
                    _keyboardLayout.LayoutParameters = layoutParams;
                    break;
            }
            mainLayout.Invalidate();
            return true;
        }
    }

}